let colors = [ 
     "black",  "maroon",
     "blue", "navy", 
     "fuchsia", "olive", 
     "green", "purple",
     "gray", "red",
     "lime",  "silver", 
     "teal", "white", 
     "yellow", "aqua",
    ];


//!изменить фон холста при нажатии кнопки
let button = document.getElementById('button');

button.addEventListener('click', function(){
    //!выберите случайное число от 0 до 6
    let index = parseInt((Math.random()*colors.length)+1);
    //!схватить холст
    let canvas = document.getElementById('canvas');

    canvas.style.background = `${colors[index]}`
})